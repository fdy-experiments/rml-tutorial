#!/bin/bash
# create out dir if not exist
mkdir -p outrdf
# iterate 
for (( i=0; i<=13000; i+=1000 ))
do
    echo 'Iterating : i= '$i
    sed -r 's/\&start=[0-9]+/\&start='$i'/g' hal-json-to-rdf_2.rml.ttl > rmlmap.ttl
    java -jar ../rmlmapper.jar -m rmlmap.ttl -o outrdf/hal-to-rdf_$i.out.ttl -s turtle -v
done