#!/bin/bash
# create out dir if not exist
mkdir -p outrdf
# iterate on resume tokens
declare -a tokens=("AoElOTczMjg=" "AoEmOTQwOTE5" "AoEmOTE2MDQx" "AoElODc4ODc=" "AoElODY3MzM=" "AoEmODQ4Nzk1" "AoEmODIxNDQ3" "AoElNzg4MzQ=" "AoEmNzY0MTQ4" "AoEmNzU1ODk1" "AoEmNzM0Mzkw" "AoEmNzIxMjky")
for i in "${tokens[@]}"
do
    echo 'Iterating : i= '$i
    sed -r 's/\&resumptionToken=[^"]+/\&resumptionToken='$i'/g' hal-oai-dc-to-rdf.rml.ttl > rmlmap.ttl
    java -jar ../rmlmapper.jar -m rmlmap.ttl -o outrdf/hal-to-rdf_$i.out.ttl -s turtle
done

#
# "AoElOTczMjg=" "AoEmOTQwOTE5" "AoEmOTE2MDQx" "AoElODc4ODc=" "AoElODY3MzM=" "AoEmODQ4Nzk1" "AoEmODIxNDQ3" "AoElNzg4MzQ=" "AoEmNzY0MTQ4" "AoEmNzU1ODk1" "AoEmNzM0Mzkw" "AoEmNzIxMjky"