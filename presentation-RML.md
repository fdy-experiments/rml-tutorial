# RML - RDF Mapping Languag

* language de mapping générique basé sur le standard W3C R2RML (Relational To RDF Mapping Language)
  * R2RML dédié au BD relationnelle (SQL-like)
  * RML dédié aux données semi-structurées, comme fichiers ou API au format CSV, JSON, XML
* développé par l'Université de Ghent (BE) depuis 2013
* solution déclarative (no-code) pour générer des triplets RDF à partir de règles de mapping exprimées en RDF en suivant le modèle de données (ontologie) [RML](https://rml.io/specs/rml/)
  

# Rappels RDF

* RDF est un modèle de graphe destiné à décrire de façon formelle les ressources Web et leurs métadonnées, de façon à permettre le traitement automatique de telles descriptions. 
* Développé par le W3C, RDF est le langage de base du Web sémantique. 
* La sémantique de RDF est basée sur les triplets permettant de décrire aussi bien des relations typées entre objets que des attributs d'objets
* Les 3 composantes d'un triplet : Sujet == Prédicat ==> Objet
* L'une des syntaxes (ou sérialisations) de ce langage est RDF/XML. D'autres syntaxes de RDF sont apparues ensuite, cherchant à rendre la lecture plus compréhensible ; c'est le cas par exemple de Turtle. Exemple:
  
```turtle
@prefix mnx: <http://data.mnemotix.com> .

mnx:FreddyL foaf:name "Freddy Limpens" ;
            foaf:knows mnx:NicoD .
```


#  Structure des règles de mapping

 Un fichier RML va permettre de définir une ou plusieurs `Triples Map`, décrivant litéralement la composition et la nature des 3 parties d'un triplets : *Sujet*, *Prédicat*, *Objet* à partir d'une référence dans la donnée source. 

Les `Triples Map` comportent E ou 3 éléments:
1. **Logical Source** : fichier ou URL d'API avec indication d'un chemin au sein de la structure de données vers les noeuds sources. 
2. **Subject Map**  : les règles de génération de l'URI des sujets des triplets générés et de leurs types
* (si nécessaire) **Predicate-Object map** : qui spécifie les triplets additionnels partant des mêmes sujets


# Types de sources prises en charges

* Principalement des fichiers ou réponses d'API semi-structurés avec comme objets de référence:
  * les colomnes pour les CSV/TSV
  * des chemins JSONPath pour les fichiers/API JSON
  * des chemins XPath pour les fichiers/API  XML
* API Rest
  * en spécifiant l'URL complète
  * pagination non supportée
* Base de données SQL => voir [R2RML](https://www.w3.org/TR/r2rml/) 

# Déploiement - Mise en oeuvre

* Règles RML sont exécutés par un mapper (RML processor) dont 2 instances codées en java sont disponibles:
  * [RML_mapper](https://github.com/RMLio/rmlmapper-java)
  * [RML_streamer](https://github.com/RMLio/RMLStreamer) variante de RML_mapper pour branchements sur des flux de streamming de données
* Intégrations possibles
  * binaire compilé pour exécution en ligne de commande, ou via orchestrateur (Airflow, Luigi, Jenkins, etc.)
  * comme librairie Java (exemple [ici](https://github.com/RMLio/rmlmapper-java/blob/master/src/test/java/be/ugent/rml/readme/ReadmeTest.java))
  * container docker disponible [rmlio/rmlmapper-java](https://hub.docker.com/r/rmlio/rmlmapper-java)


# RML is not an harvester   

RML.io ne traite que le *mapping* et nécessite donc de coder en amont le *harvesting*, ie:
  * authentification basée sur login/mdp et/ou token (cf twitter) 
  * timer pour prise en compte throttling
  * mise en cache de certaines infos de contexte : sources de données, prefixes, typage, etc
  * itération selon modalité de pagination : extraction du nombre de résultats, from/to, resumption token, etc

# Pré-processing des données

* Dans certains cas, nécessité de traiter les données avant le mapping. Ex. Retirer un # dans un id `#1234` 
* RML permet l'usage de [fonctions disponibles dans le mapper](https://rml.io/docs/rmlmapper/default-functions/) comme `replaceChars`. 
* l'usage de ces fonctions complexifie cependant le mapping et comporte des limites
  * chainage compliqué voir impossible
  * certaines fonctions n'acceptent pas de valeurs nulles en input
  * panel de fonctions limité, mais possibilité de coder ses propres fonctions et de les inclures dynamiquement en parallèle du binaire compilé


# Conclusion

Pros :  
  * language déclaratif homogène au format cible, le RDF
  * prise en main rapide et simple pour qui maitrise déjà RDF
  * étend R2RML recommendation W3C
  * suite d'outils complètes du validateur, editeur, au processeur de mappings RML
  * communauté de contributeurs modeste, centré en Belgique, mais soutenue par Université de Ghent et active avec rythme de release stable: ~ 1 major / an, dernière en date juillet 2023

Cons :
  * pas une réelle solution pour moissonage d'API ; pas d'authentification, ni pagination
  * pre-processing des données limité
  * documentation encore perfectible
  * niveau d'adoption encore un peu faible 


# Annexe

Exemples et démos : cf [https://gitlab.com/fdy-experiments/rml-tutorial](https://gitlab.com/fdy-experiments/rml-tutorial)