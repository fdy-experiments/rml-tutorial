#!/bin/bash
# create out dir if not exist
mkdir -p outrdf
# iterate 
for (( i=0; i<=13450; i+=100 ))
do
    echo 'Iterating : i= '$i
    sed -r 's/\&start=[0-9]+/\&start='$i'/g' hal-xmltei-to-rdf.rml.ttl > rmlmap.ttl
    java -jar ../rmlmapper.jar -m rmlmap.ttl -o outrdf/hal-to-rdf_$i.out.ttl -s turtle -v
done

#
# "AoElOTczMjg=", "AoEmOTQwOTE5", "AoEmOTE2MDQx", "AoElODc4ODc=", "AoElODY3MzM=", "AoEmODQ4Nzk1", "AoEmODIxNDQ3", "AoElNzg4MzQ=", "AoEmNzY0MTQ4", "AoEmNzU1ODk1", "AoEmNzM0Mzkw", "AoEmNzIxMjky"ê