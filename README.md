# RML Tutorial


## wot-json

[This](https://rml.io/docs/rml/tutorials/wot-json/) tutorial but adapated to  Hal Solr `search` api endpoint 

* example call : https://api.archives-ouvertes.fr/search/?q=*%3A*&fq=instStructAcronym_s:MNHN&fq=openAccess_bool:true&wt=json&fl=title_s,releasedDate_tdate,uri_s,openAccess_bool,abstract_s,fileMain_s,label_s
* target RDF : target-rdf.ttl
* Rules : hal-to-rdf_1.rml.ttl
* command to run the mapping : `java -jar rmlmapper.jar -m hal-to-rdf_1.rml.ttl -o hal-to-rdf.out.ttl -s turtle -v`
* Output :  hal-to-rdf.out.ttl     

## Temps d'exécution

### hal-oaipmh-dc-xml

Pour 100 records : 
16:54:45.074 - 16:54:43.536 = 1,538s 

=> 30k records ~ 8mn
=> 600k ~ 2h30

## Usage de fonction

### string_replaceChars 

```turtle
<#replaceCommaSpaceToUnderscore> 
  fnml:functionValue [ # The object is the result of the function
    rml:logicalSource <#EntriesLogicalSource> ;  # Use the same data source for input
    rr:predicateObjectMap [
        rr:predicate fno:executes ;  # Choose the function…
        rr:objectMap [ rr:constant  grel:string_replaceChars ] 
      ],
      [ # Use as input the "name" reference
        rr:predicate grel:valueParameter ;
        rr:objectMap [ rml:reference "metadata/oai_dc:dc/dc:creator" ]        
      ],
      [
        rr:predicate grel:p_string_find ;
        rr:objectMap [ rr:constant ", " ]         # look for `#`
      ],
      [
        rr:predicate grel:p_string_replace ;
        rr:objectMap [ rr:constant "__" ]         # replace by empty string ""
      ]
  ];
  rr:termType rr:IRI
  .
  ```

  ### Slugify

```turtle
  <#slugifyDCCreator> 
  fnml:functionValue [ # The object is the result of the function
    rml:logicalSource <#EntriesLogicalSource> ;  # Use the same data source for input
    rr:predicateObjectMap [
        rr:predicate fno:executes ;  # Choose the function…
        rr:objectMap [ rr:constant  <http://example.com/idlab/function/slugify> ] 
      ],
      [ # Use as input the "name" reference
        rr:predicate <http://example.com/idlab/function/str> ;
        rr:objectMap [ rml:reference "metadata/oai_dc:dc/dc:creator" ]        
      ]
  ];
  rr:termType rr:IRI
  .
  ```